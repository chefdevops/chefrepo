name 'terragon_nodejs'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures terragon_nodejs'
long_description 'Installs/Configures terragon_nodejs'
version '0.1.0'

depends 'yum-epel'
#depends 'build-essential'


%w(debian ubuntu centos redhat scientific oracle amazon smartos mac_os_x).each do |os|
  supports os
end
