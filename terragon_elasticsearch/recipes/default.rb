#
# Cookbook Name:: terragon_nginx_phpfpm
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

case node['platform_family']
when 'debian'
  include_recipe 'apt'

  apt_repository 'node.js' do
    uri node['nodejs']['repo']
    distribution node['lsb']['codename']
    components ['main']
    keyserver node['nodejs']['keyserver']
    key node['nodejs']['key']
  end
when 'rhel'
  include_recipe 'yum-epel'
end



#package 'nginx'
#package 'php-fpm'

#package 'java-1.8.0-openjdk.x86_64'

package 'java-1.8.0-openjdk'

rpm_package 'elasticsearch' do
	action :install
	#source 'https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.3.noarch.rpm'
	source 'https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/rpm/elasticsearch/2.3.5/elasticsearch-2.3.5.rpm'
end


#template '/etc/nginx/nginx.conf' do
#  source 'nginx.conf.erb'
#end

