#
# Cookbook Name:: terragon_nginx_phpfpm
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

case node['platform_family']
when 'debian'
  include_recipe 'apt'

  apt_repository 'node.js' do
    uri node['nodejs']['repo']
    distribution node['lsb']['codename']
    components ['main']
    keyserver node['nodejs']['keyserver']
    key node['nodejs']['key']
  end
when 'rhel'
  include_recipe 'yum-epel'
end



package 'nginx'
package 'php-fpm'

template '/etc/nginx/nginx.conf' do
  source 'nginx.conf.erb'
end

