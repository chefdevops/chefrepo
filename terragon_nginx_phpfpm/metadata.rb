name 'terragon_nginx_phpfpm'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'all_rights'
description 'Installs/Configures terragon_nginx_phpfpm'
long_description 'Installs/Configures terragon_nginx_phpfpm'
version '0.1.0'


depends 'yum-epel'
#depends 'build-essential'
#
#
%w(debian ubuntu centos redhat scientific oracle amazon smartos mac_os_x).each do |os|
  supports os
 end
