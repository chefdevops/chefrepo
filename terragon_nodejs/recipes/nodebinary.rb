Chef::Recipe.send(:include, NodeJs::Helper)

node.force_override['nodejs']['install_method'] = 'binary' # ~FC019


arch = if node['kernel']['machine'] =~ /armv6l/
         'arm-pi' # assume a raspberry pi
       else
         node['kernel']['machine'] =~ /x86_64/ ? 'x64' : 'x86'
       end

version = "v#{node['nodejs']['version']}/"
prefix = node['nodejs']['prefix_url'][node['nodejs']['engine']]

if node['nodejs']['engine'] == 'iojs'
  filename = "iojs-v#{node['nodejs']['version']}-linux-#{arch}.tar.gz"
  archive_name = 'iojs-binary'
  binaries = ['bin/iojs', 'bin/node']
else
  filename = "node-v#{node['nodejs']['version']}-linux-#{arch}.tar.gz"
  archive_name = 'nodejs-binary'
  binaries = ['bin/node']
end

binaries.push('bin/npm') if node['nodejs']['npm']['install_method'] == 'embedded'

if node['nodejs']['binary']['url']
  nodejs_bin_url = node['nodejs']['binary']['url']
  checksum = node['nodejs']['binary']['checksum']
else
  nodejs_bin_url = ::URI.join(prefix, version, filename).to_s
  checksum = node['nodejs']['binary']['checksum']["linux_#{arch}"]
end

ark archive_name do
  url nodejs_bin_url
  version node['nodejs']['version']
  checksum checksum
  has_binaries binaries
  action :install
end
