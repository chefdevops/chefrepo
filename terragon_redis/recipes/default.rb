#
# Cookbook Name:: terragon_nginx_phpfpm
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

case node['platform_family']
when 'debian'
  include_recipe 'apt'

  apt_repository 'node.js' do
    uri node['nodejs']['repo']
    distribution node['lsb']['codename']
    components ['main']
    keyserver node['nodejs']['keyserver']
    key node['nodejs']['key']
  end
when 'rhel'
  include_recipe 'yum-epel'
end



directory '/etc/redis' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end


template '/etc/redis/redis.conf' do
  source 'redis.conf.erb'
end



bash 'build-redis-3.2' do
	code <<-EOF
	wget http://download.redis.io/releases/redis-3.2.0.tar.gz
	tar xzf redis-3.2.0.tar.gz
	rm -f redis-3.2.0.tar.gz
	cd redis-3.2.0
	make distclean
	make
	mkdir -p /etc/redis /var/lib/redis /var/redis/6379
	cp src/redis-server src/redis-cli /usr/local/bin
		
	wget https://raw.githubusercontent.com/saxenap/install-redis-amazon-linux-centos/master/redis-server
	mv redis-server /etc/init.d
	chmod 755 /etc/init.d/redis-server
	chkconfig --add redis-server
	chkconfig --level 345 redis-server on
	service redis-server start
	EOF
end
